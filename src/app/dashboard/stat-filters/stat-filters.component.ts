import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { StatFiltersService } from '../stat-filters.service';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.scss']
})
export class StatFiltersComponent {

  group: FormGroup;

  constructor(svc: StatFiltersService) {
    this.group = svc.group;
  }

}
