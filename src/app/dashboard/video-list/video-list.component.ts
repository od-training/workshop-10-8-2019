import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { Video } from '../types';
import { VideoDataService } from '../video-data.service';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent {

  videos: Observable<Video[]>;
  selectedVideo: Observable<string>;

  constructor(route: ActivatedRoute, svc: VideoDataService) {
    this.selectedVideo = route.queryParamMap.pipe(map(params => params.get('id') || ''));
    this.videos = svc.getVideos();
  }

}
