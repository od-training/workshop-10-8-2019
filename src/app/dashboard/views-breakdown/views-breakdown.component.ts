import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { Video } from '../types';
import { VideoDataService } from '../video-data.service';

@Component({
  selector: 'app-views-breakdown',
  templateUrl: './views-breakdown.component.html',
  styleUrls: ['./views-breakdown.component.scss']
})
export class ViewsBreakdownComponent {

  selectedVideo: Observable<Video>;

  constructor(svc: VideoDataService) {
    this.selectedVideo = svc.selectedVideo;
  }

}
