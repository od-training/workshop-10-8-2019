import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ParamMap, ActivatedRoute } from '@angular/router';
import { map, shareReplay, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { Video } from './types';

function uppercaseVideoTitles(videos: Video[]) {
  return videos.map(video => ({
    ...video,
    title: video.title.toUpperCase()
  }));
}

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  selectedVideo: Observable<Video>;

  constructor(private http: HttpClient, route: ActivatedRoute) {
    this.selectedVideo = route.queryParamMap.pipe(
      map((params: ParamMap) => params.get('id') || ''),
      switchMap((id: string) => this.getVideo(id)),
      shareReplay(1),
    );
  }

  getVideos(): Observable<Video[]> {
    return this.http
      .get<Video[]>('http://localhost:8085/videos') // or 'https://api.angularbootcamp.com/videos'
      .pipe(map(videos => uppercaseVideoTitles(videos)));
  }

  getVideo(id: string): Observable<Video> {
    return this.http.get<Video>(`http://localhost:8085/videos/${id}`);
  }

}
