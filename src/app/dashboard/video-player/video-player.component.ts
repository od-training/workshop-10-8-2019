import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

import { Video } from '../types';
import { VideoDataService } from '../video-data.service';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent {

  video: Observable<Video>;
  url: Observable<SafeResourceUrl>;

  constructor(svc: VideoDataService, sanitizer: DomSanitizer) {
    this.video = svc.selectedVideo;
    this.url = svc.selectedVideo.pipe(
      map(v => `https://www.youtube.com/embed/${v.id}`),
      map(url => sanitizer.bypassSecurityTrustResourceUrl(url)),
    );
  }

}
