import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class StatFiltersService {

  get group() {
    return this.formGroup;
  }

  private formGroup: FormGroup;

  constructor(fb: FormBuilder) {
    this.formGroup = fb.group({
      region: ['all'],
      fromDate: ['1995-01-01'],
      toDate: ['2019-11-01'],
      under18: [true],
      '18to40': [true],
      '40to60': [true],
      over60: [true],
    });
  }

}
