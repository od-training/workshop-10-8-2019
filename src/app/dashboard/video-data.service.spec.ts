import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { EMPTY } from 'rxjs';

import { VideoDataService } from './video-data.service';

describe('VideoDataService', () => {
  let httpTestingController: HttpTestingController;
  let service: VideoDataService;

  beforeEach(() => {

    const activatedRoute = jasmine.createSpyObj('ActivatedRoute', ['queryParamMap']);
    activatedRoute.queryParamMap.and.returnValue(EMPTY);

    TestBed.configureTestingModule({
      providers: [
        {
          provide: ActivatedRoute,
          useValue: activatedRoute
        },
      ],
      imports: [
        HttpClientTestingModule,
      ]
    });

    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(VideoDataService);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
